#!/usr/bin/env bash

doBuild(){
    source ../base_build_android.sh

    if [ "$ARCH" = "arm64-v8a" ]; then
        MY_HOST=aarch64-linux
    elif [ "$ARCH" = "armeabi-v7a" ]; then
        MY_HOST=arm-linux
    else
        MY_HOST=arm-linux
    fi

    #编译的库名称
    LIBNAME="x264"

    make clean

    ./configure \
    --prefix=$(pwd)/build_out/Android/$ARCH \
    --cross-prefix=${CROSS_PREFIX} \
    --host=${MY_HOST} \
    --enable-pic \
    --enable-strip \
    --enable-static \
    --sysroot=$PLATFORM \

    #编译 并将编译结果拷贝到include和libs到对应的总目录
    makeAndCopy

    echo "---------开始拷贝到 ffmpeg external_libs---------"

    mkdir -p ${PREFIX_OUT_PATH}/ffmpeg/external_libs/${LIBNAME}/ && cp -r build_out/Android/ "$_"
}

archs=(
    'armeabi-v7a'
    'arm64-v8a'
)

num=${#archs[@]}
for ((i=0;i<$num;i++))
do
    doBuild ${archs[i]}
done